﻿using System;
using UnityEngine;

[System.Serializable]
public class ChooseAnswer
{
    public string artist;
    public string title;
}

[System.Serializable]
public class Song
{
    public string id;
    public string title;
    public string artist;
    public string picture;
    public string sample;
}

[System.Serializable]
public class Question
{
    public string id;
    public string answerIndex;
    public ChooseAnswer[] choices;
    public Song song;
}

[System.Serializable]
public class Playlist
{
    public string id;
    public Question[] questions;
    public string playlist;
}

[System.Serializable]
public class Playlists
{
    public Playlist[] playlists;
}

public class JsonReader
{

    private static JsonReader jsonReader;
    public static JsonReader Instance()
    {
        if (jsonReader == null)
        {
            jsonReader = new JsonReader();

            if (jsonReader == null)
            {
                Debug.LogError("JsonReader inactive or missing from unity scene.");
            }
        }

        return jsonReader;
    }

    public Playlists JsonReaderData(TextAsset jsonFile)
    {
        Debug.Log("JSONReaderData : " + jsonFile.name);
        //Debug.Log("JSONReaderData : " + jsonFile.text);
        Playlists playlistInJson = JsonUtility.FromJson<Playlists>(jsonFile.text);

        //foreach (Playlist playlist in playlistInJson.playlists)
        //{
        //    Debug.Log("Found playlist: " + playlist.id + " " + playlist.playlist);
        //}

        return playlistInJson;
    }
}