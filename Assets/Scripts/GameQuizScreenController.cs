﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

struct SongData
{
    public string id;
    public Sprite pictureSpriteData;
    public AudioClip audioSourceData;
}

public class GameQuizScreenController : MonoBehaviour
{
    public GameObject answerPack;
    public Button nextQuestion;
    public Text score;
    public Button playSong;
    public AudioSource audioSource;
    public GameObject thisImg;
    public GameObject panelLoading;

    [HideInInspector]
    public int numQuestion = 0;
    [HideInInspector]
    public bool isAnswered = false;

    int answerIndex = -1;
    bool isLoadingData = false;

    Question[] allQuestion;
    SongData[] songDatas;
    int dataLoading = 0;

    public void InitGameQuizScreen()
    {
        numQuestion = 0;
        GameManager.Instance.scoreNumber = 0;
        GameManager.Instance.totalQuestion = 0;
        score.text = "Score : " + GameManager.Instance.scoreNumber + "/" + numQuestion;
        isAnswered = false;
        answerIndex = -1;
        isLoadingData = true;
        panelLoading.SetActive(true);
        nextQuestion.gameObject.SetActive(false);
        audioSource.Stop();
        for (int i = 0; i < 4; i++)
        {
            //reset color of btn answerChoice
            answerPack.transform.GetChild(i).GetComponent<Image>().color = Color.blue;
        }
        LoadAllDataFromWeb();

        //reset value of saved chosenAnswer
        for (int i = 0; i < GameManager.Instance.chosenAnswer.Length; i++)
        {
            GameManager.Instance.chosenAnswer[i] = -1;
        }
    }

    void Update()
    {
        if (audioSource.isPlaying)
            playSong.GetComponentInChildren<Text>().text = "Playing : " + audioSource.time.ToString("F1") + " / " + audioSource.clip.length + " sec";
        else
            playSong.GetComponentInChildren<Text>().text = "Play Song";

        if (dataLoading > 0)
        {
            panelLoading.GetComponentInChildren<Text>().text = "Loading ...";
        }
    }

    public void PlayAudio()
    {
        audioSource.Play();
    }

    private IEnumerator LoadImgFromURL(string url, int index)
    {
        //Debug.Log("Loading ImgFromURL ... ");
        Sprite sp = Resources.Load<Sprite>("Textures/loading") as Sprite;
        thisImg.GetComponent<Image>().sprite = sp;
        WWW wwLoader = new WWW(url);
        yield return wwLoader;

        //Debug.Log("Loaded");
        Sprite mySprite;
        mySprite = Sprite.Create(wwLoader.texture, new Rect(0.0f, 0.0f, wwLoader.texture.width, wwLoader.texture.height), new Vector2(0.5f, 0.5f), 100.0f);
        dataLoading--;
        songDatas[index].pictureSpriteData = mySprite;
        if (dataLoading <= 0)
        {
            AllDataLoaded();
        }
    }

    IEnumerator LoadMusicURL(string url, int index)
    {
        //Debug.Log("Loading MusicURL ... ");
        WWW wwLoader = new WWW(url);
        yield return wwLoader;

        //Debug.Log("Loaded");
        dataLoading--;
        songDatas[index].audioSourceData = wwLoader.GetAudioClip();
        if (dataLoading <= 0)
        {
            AllDataLoaded();
        }
    }

    public void LoadQuestion()
    {
        if (numQuestion >= GameManager.Instance.totalQuestion && GameManager.Instance.totalQuestion > 0)
        {
            //go to result screen
            ScreenManager.Instance.ActiveScreen("ResultScreen");
            songDatas = null;
            //need free memory of array here
            return;
        }

        //load answer from json
        Playlist pl = GameManager.Instance.playlistInJson.playlists[WelcomeScreenController.Instance.indexPlaylistChosen];
        Question curQuestion = pl.questions[numQuestion];
        ChooseAnswer[] answer = curQuestion.choices;
        for (int i = 0; i < 4; i++)
        {
            string txt = "Artist: " + answer[i].artist + " , Title: " + answer[i].title;
            answerPack.transform.GetChild(i).GetComponentInChildren<Text>().text = txt;
            //reset color of btn answerChoice
            answerPack.transform.GetChild(i).GetComponent<Image>().color = Color.blue;
        }
        answerIndex = int.Parse(curQuestion.answerIndex);
        GameManager.Instance.totalQuestion = pl.questions.Length;

        //load image from URL
        thisImg.GetComponent<Image>().color = Color.white;
        if (curQuestion.id == songDatas[numQuestion].id)
            thisImg.GetComponent<Image>().sprite = songDatas[numQuestion].pictureSpriteData;
        else
            Debug.Log("Something wrong when load pictureSpriteData downloaded");
        //load song from URL
        if (audioSource.clip != null)
        {
            //unload the existing clip
            audioSource.Stop();
            AudioClip curClip = audioSource.clip;
            audioSource.clip = null;
            curClip.UnloadAudioData();
            DestroyImmediate(curClip, true);
        }

        audioSource.loop = false;
        //audioSource.volume = .2f;
        if (curQuestion.id == songDatas[numQuestion].id)
            audioSource.clip = songDatas[numQuestion].audioSourceData;
        else
            Debug.Log("Something wrong when load audioSource downloaded");
        audioSource.Play();
    }

    public void NextQuestion()
    {
        numQuestion++;
        LoadQuestion();
        nextQuestion.gameObject.SetActive(false);
        isAnswered = false;
    }

    public void AnsweredQuestion(int numChoice)
    {
        Debug.Log("GameQuizScreenController AnsweredQuestion : " + numChoice);
        GameManager.Instance.chosenAnswer[numQuestion] = numChoice;
        if (isAnswered == false && numQuestion < GameManager.Instance.totalQuestion)
        {
            nextQuestion.gameObject.SetActive(true);
            isAnswered = true;
            answerPack.transform.GetChild(numChoice).GetComponent<Image>().color = Color.red;

            //show correct answer
            Debug.Log("GameQuizScreenController answerIndex : " + answerIndex);
            answerPack.transform.GetChild(answerIndex).GetComponent<Image>().color = Color.green;
            if (answerIndex == numChoice)
            {
                GameManager.Instance.scoreNumber++;
            }
            score.text = "Score : " + GameManager.Instance.scoreNumber + "/" + (numQuestion + 1);
        }
    }

    void LoadAllDataFromWeb()
    {
        if (songDatas != null)
        {
            Debug.Log("LoadAllDataFromWeb : Data is already loaded.");
            return;
        }

        Playlist pl = GameManager.Instance.playlistInJson.playlists[WelcomeScreenController.Instance.indexPlaylistChosen];
        Question[] allQuestion = pl.questions;
        songDatas = new SongData[allQuestion.Length];
        int i = 0;

        foreach (Question q in allQuestion)
        {
            string urlImg = q.song.picture;
            string urlAudio = q.song.sample;

            songDatas[i].id = q.id;
            StartCoroutine(LoadImgFromURL(urlImg, i));
            dataLoading++;
            StartCoroutine(LoadMusicURL(urlAudio, i));
            dataLoading++;
            i++;
        }
    }

    void AllDataLoaded()
    {
        panelLoading.SetActive(false);
        LoadQuestion();
    }
}
