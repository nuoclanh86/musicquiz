﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DanielLochner.Assets.SimpleScrollSnap;

public class ResultScreenController : MonoBehaviour
{
    public Text score;
    public GameObject resultScrollView;
    [SerializeField]
    protected GameObject panel;

    public void InitResultScreen()
    {
        score.text = "Score : " + GameManager.Instance.scoreNumber + "/" + GameManager.Instance.totalQuestion;
        //remove all old child of scrollview
        for (int i = 0; i < 10; i++)
        {
            resultScrollView.GetComponent<DynamicContentController>().Remove(0);
        }
        //add new items
        for (int i = 0; i < GameManager.Instance.totalQuestion; i++)
        {
            panel.GetComponent<AnswerchoicePackPanel>().LoadQuestionToPack(i, GameManager.Instance.chosenAnswer[i]);
            resultScrollView.GetComponent<DynamicContentController>().Add(panel, i);
        }
    }

    public void PlayAgain()
    {
        GameManager.Instance.scoreNumber = 0;
        GameManager.Instance.totalQuestion = 0;
        ScreenManager.Instance.ActiveScreen("WelcomeScreen");
    }
}
