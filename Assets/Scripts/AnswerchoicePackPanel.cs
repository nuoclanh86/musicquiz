﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswerchoicePackPanel : MonoBehaviour
{

    // Start is called before the first frame update
    //void Start()
    //{

    //}

    // Update is called once per frame
    //void Update()
    //{

    //}

    public void LoadQuestionToPack(int numQuestion, int numChoice)
    {
        //Debug.Log("LoadQuestionToPack");
        GameObject answerPack = this.transform.GetChild(0).gameObject;

        if (numQuestion >= GameManager.Instance.totalQuestion && GameManager.Instance.totalQuestion > 0)
        {
            Debug.Log("LoadQuestionToPack error, something wrong here");
            return;
        }

        //load answer from json
        Playlist pl = GameManager.Instance.playlistInJson.playlists[WelcomeScreenController.Instance.indexPlaylistChosen];
        Question curQuestion = pl.questions[numQuestion];
        ChooseAnswer[] answer = curQuestion.choices;
        int answerIndex = int.Parse(curQuestion.answerIndex);
        for (int i = 0; i < 4; i++)
        {
            string txt = "  Artist: " + answer[i].artist + " , Title: " + answer[i].title;
            answerPack.transform.GetChild(i).GetComponentInChildren<Text>().text = txt;
            //reset color to default
            answerPack.transform.GetChild(i).GetComponent<Image>().color = Color.blue;
            //show user answer if it's wrong
            if (answerIndex != GameManager.Instance.chosenAnswer[numQuestion] && i == GameManager.Instance.chosenAnswer[numQuestion])
                answerPack.transform.GetChild(i).GetComponent<Image>().color = Color.red;
        }

        //show correct answer
        answerPack.transform.GetChild(answerIndex).GetComponent<Image>().color = Color.green;
    }
}
