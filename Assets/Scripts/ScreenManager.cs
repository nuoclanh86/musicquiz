﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenManager : MonoBehaviour
{

    public static ScreenManager Instance { get; private set; }
    void Awake()
    {
        // Save a reference to the AudioManager component as our //singleton instance.
        Instance = this;
    }

    public GameObject[] allScreenUI;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ActiveScreen(string screenName)
    {
        foreach(GameObject screen in allScreenUI)
        {
            //Debug.Log("screen = " + screen.name);
            if (screen.name != screenName)
            {
                //Debug.Log("Destroy screen = " + screen.name);
                //Destroy(screen.gameObject);
                screen.gameObject.SetActive(false);
            }
            if (screen.name == screenName)
            {
                //Debug.Log("ActiveScreen screen = " + screen.name);
                //Instantiate(screen);
                screen.gameObject.SetActive(true);
            }
        }
        //init screen
        if (screenName == "WelcomeScreen")
        {
            GameManager.Instance.curScreen = GameScreens.WelcomeScreen;
            allScreenUI[0].GetComponent<WelcomeScreenController>().InitWelcomeUI();
        }
        else if (screenName == "GameQuizScreen")
        {
            GameManager.Instance.curScreen = GameScreens.GameQuizScreen;
            allScreenUI[1].GetComponent<GameQuizScreenController>().InitGameQuizScreen();
        }
        else if (screenName == "ResultScreen")
        {
            GameManager.Instance.curScreen = GameScreens.ResultScreen;
            allScreenUI[2].GetComponent<ResultScreenController>().InitResultScreen();
        }
        else
            Debug.LogError("This screen : " + screenName + " not in list");
    }
}
