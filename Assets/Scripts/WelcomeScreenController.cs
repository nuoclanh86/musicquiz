﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WelcomeScreenController : MonoBehaviour
{
    public ScrollRect scrollView;
    public GameObject scrollContent;
    public GameObject scrollItemPrefab;

    bool isPlaylistLoaded = false;
    [HideInInspector]
    public int indexPlaylistChosen;

    public static WelcomeScreenController Instance { get; private set; }
    void Awake()
    {
        // Save a reference to the AudioManager component as our //singleton instance.
        Instance = this;
    }

    public void InitWelcomeUI()
    {
        if (isPlaylistLoaded == false)
        {
            GameManager.Instance.playlistInJson = JsonReader.Instance().JsonReaderData(GameManager.Instance.jsonFile);
            int index = 0;
            foreach (Playlist playlist in GameManager.Instance.playlistInJson.playlists)
            {
                //Debug.Log("Found playlist: " + playlist.id + " --- " + playlist.playlist);
                GenrateItem(playlist, index);
                index++;
            }
        }
        scrollView.verticalNormalizedPosition = 1;

        indexPlaylistChosen = 0;
        UpdateWelcomeUI(indexPlaylistChosen);
    }

    void GenrateItem(Playlist playlist, int index)
    {
        //Debug.Log("GenrateItem");
        //GameObject playlistItemObj = scrollItemPrefab.Spawn();
        GameObject playlistItemObj = Instantiate(scrollItemPrefab); // temp , to do ...

        //load playlist title
        playlistItemObj.transform.GetChild(0).GetComponent<Text>().text = playlist.playlist;
        playlistItemObj.GetComponent<PlaylistItem>().SetItemIndex(index);
        //load image
        Sprite sp = Resources.Load<Sprite>("Textures/backgrounds/playlistitem0" + index) as Sprite;
        playlistItemObj.GetComponent<Image>().sprite = sp;

        playlistItemObj.transform.SetParent(scrollContent.transform, false);
        isPlaylistLoaded = true;
    }

    public void UpdateWelcomeUI(int index)
    {
        foreach (Transform child in scrollContent.transform)
        {
            child.GetComponent<PlaylistItem>().UpdateUI(index);
            indexPlaylistChosen = index;
        }
    }
}
