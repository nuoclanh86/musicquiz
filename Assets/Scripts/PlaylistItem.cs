﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlaylistItem : MonoBehaviour
{
    int thisIndex = -1;

    // Update is called once per frame
    public void UpdateUI(int index)
    {
        if (thisIndex == index)
            this.transform.GetChild(1).gameObject.SetActive(true);
        else
            this.transform.GetChild(1).gameObject.SetActive(false);
    }

    public void SetItemIndex(int i)
    {
        thisIndex = i;
    }

    public void UpdateIndexChosen()
    {
        WelcomeScreenController.Instance.UpdateWelcomeUI(thisIndex);
    }
}
