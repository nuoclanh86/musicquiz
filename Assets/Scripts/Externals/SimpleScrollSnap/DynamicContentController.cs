﻿// Simple Scroll-Snap
// Version: 1.2.0
// Author: Daniel Lochner

using System;
using UnityEngine;
using UnityEngine.UI;

namespace DanielLochner.Assets.SimpleScrollSnap
{
    public class DynamicContentController : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        protected GameObject toggle;
        //panel, addInput, removeInput;

        private float toggleWidth;
        private SimpleScrollSnap sss;
        #endregion

        #region Methods
        private void Awake()
        {
            sss = GetComponent<SimpleScrollSnap>();
            toggleWidth = toggle.GetComponent<RectTransform>().sizeDelta.x * (Screen.width / 2048f); ;
        }

        public void Add(GameObject panel, int index)
        {
            //Pagination
            Instantiate(toggle, sss.pagination.transform.position + new Vector3((toggleWidth * 2) * (sss.NumberOfPanels + 1), 0, 0), Quaternion.identity, sss.pagination.transform);
            sss.pagination.transform.position -= new Vector3(toggleWidth / 2f, 0, 0);

            //Panel
            //panel.GetComponent<Image>().color = new Color(UnityEngine.Random.Range(0, 255) / 255f, UnityEngine.Random.Range(0, 255) / 255f, UnityEngine.Random.Range(0, 255) / 255f);
            sss.Add(panel, index);
        }

        public void Remove(int index)
        {
            Debug.Log("Remove NumberOfPanels" + sss.NumberOfPanels);
            if (sss.NumberOfPanels > 0)
            {
                //Pagination
                DestroyImmediate(sss.pagination.transform.GetChild(sss.NumberOfPanels - 1).gameObject);
                sss.pagination.transform.position += new Vector3(toggleWidth / 2f, 0, 0);

                //Panel
                sss.Remove(index);
            }
        }
        #endregion
    }
}