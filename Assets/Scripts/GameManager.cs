﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameScreens
{
    WelcomeScreen,
    GameQuizScreen,
    ResultScreen
}

public class GameManager : MonoBehaviour
{
    public TextAsset jsonFile;
    public GameObject screenManager;

    [HideInInspector]
    public Playlists playlistInJson;
    [HideInInspector]
    public int scoreNumber;
    [HideInInspector]
    public int totalQuestion;
    [HideInInspector]
    public int[] chosenAnswer;
    [HideInInspector]
    public GameScreens curScreen;
    
    public static GameManager Instance { get; private set; }
    void Awake()
    {
        // Save a reference to the AudioManager component as our //singleton instance.
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        InitGameManager();
    }

    void InitGameManager()
    {
        Debug.Log("InitGameManager");
        curScreen = GameScreens.WelcomeScreen;
        chosenAnswer = new int[99];
        screenManager.GetComponent<ScreenManager>().ActiveScreen("WelcomeScreen");
    }
}
